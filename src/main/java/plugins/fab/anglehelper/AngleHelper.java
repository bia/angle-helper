package plugins.fab.anglehelper;

import icy.gui.dialog.MessageDialog;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginImageAnalysis;
import icy.sequence.Sequence;

/**
 * @author Fabrice de Chaumont
 */
public class AngleHelper extends Plugin implements PluginImageAnalysis {

	@Override
	public void compute() {

		Sequence sequence = getFocusedSequence();
		
		if ( sequence!=null )
		{
			new AnglePainter( getFocusedSequence() );
		}else
		{
			MessageDialog.showDialog("Please open an image first.", MessageDialog.INFORMATION_MESSAGE );
		}
		
	}

}
